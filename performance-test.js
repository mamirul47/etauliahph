import {sleep} from 'k6';
import http from 'k6/http';

export const options = {
    duration: '1m',
    vus: 10,
}

export default function(){
    http.get('http://139-162-50-88.ip.linodeusercontent.com');
    sleep(3);
}
