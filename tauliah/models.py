from django.db import models

# Create your models here.


class TP(models.Model):
    cawangan = models.TextField('Cawangan',blank=True,null=True,default="TB")
    kelas = models.TextField('Kelas Latihan',blank=True,null=True,default="TB")
    nama = models.TextField('Nama',blank=True,null=True,default="TB")
    noKP = models.TextField('noKP',blank=True,null=True)
    status = models.TextField('Status',blank=True,null=True,default="TB")
    penyelia = models.TextField('Nama Penyelia',blank=True,null=True,default="TB")
    tahun = models.TextField('Tahun',blank=True,null=True,default="1445")
    jenis = models.TextField('Jenis Kelas',blank=True,null=True,default="AWAM")
    jantina = models.TextField('Jantina',blank=True,null=True,default="L")

    def __str__(self):
        return str(self.nama) +" - "+ str(self.kelas)
