# Generated by Django 4.2.4 on 2023-09-01 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tauliah', '0004_tp_jenis_alter_tp_nokp'),
    ]

    operations = [
        migrations.AddField(
            model_name='tp',
            name='jantina',
            field=models.TextField(blank=True, default='L', null=True, verbose_name='Jantina'),
        ),
    ]
