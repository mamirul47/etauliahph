from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import TP
from unfold.admin import ModelAdmin 
from unfold.contrib.import_export.forms import ExportForm, ImportForm, SelectableFieldsExportForm

@admin.register(TP)
class AdminTP(ModelAdmin,ImportExportModelAdmin):
    import_form_class = ImportForm
    export_form_class = ExportForm
    list_display = ["cawangan","kelas", "nama", "noKP","status","tahun"]
    search_fields = ['nama','noKP','cawangan','kelas','status']
    list_filter = ["cawangan","kelas"]

# Register your models here.
#admin.site.register(TP,AdminTP)